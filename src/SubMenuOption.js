import React from 'react';
import PropTypes from 'prop-types';
import { NavLink as RouterLink } from 'react-router-dom';

import Icon from '@material-ui/core/Icon';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  chosen: {
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  collapseIcon: {
    fontSize: '1.75rem',
  },
}));

function SubMenuOption({collapse, exact, icon, name, path}) {
  const classes = useStyles();
  const CollisionLink = React.forwardRef((props, ref) => (
    <RouterLink innerRef={ref} to={path} {...props} />
  ));
  if (collapse) {
    return (
      <ListItem
        button
        exact={exact}
        activeClassName={classes.chosen}
        component={CollisionLink}
        to={path}>
        <Tooltip title={name} placement="right">
          <Icon className={classes.collapseIcon} color="action">{icon}</Icon>
        </Tooltip>
      </ListItem>
    );
  }
  return (
    <ListItem
      button
      exact={exact}
      activeClassName={classes.chosen}
      component={CollisionLink}
      to={path}>
      <ListItemIcon>
        <Icon>{icon}</Icon>
      </ListItemIcon>
      <ListItemText primary={name} />
    </ListItem>
  );
}

SubMenuOption.defaultProps = {
  collapse: false,
  exact: false,
};

SubMenuOption.propTypes = {
  collapse: PropTypes.bool,
  exact: PropTypes.bool,
  icon: PropTypes.node,
  name: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

export default SubMenuOption;
