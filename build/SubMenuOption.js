"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactRouterDom = require("react-router-dom");

var _Icon = _interopRequireDefault(require("@material-ui/core/Icon"));

var _ListItem = _interopRequireDefault(require("@material-ui/core/ListItem"));

var _ListItemIcon = _interopRequireDefault(require("@material-ui/core/ListItemIcon"));

var _ListItemText = _interopRequireDefault(require("@material-ui/core/ListItemText"));

var _Tooltip = _interopRequireDefault(require("@material-ui/core/Tooltip"));

var _styles = require("@material-ui/core/styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    chosen: {
      backgroundColor: 'rgba(0,0,0,0.1)'
    },
    collapseIcon: {
      fontSize: '1.75rem'
    }
  };
});

function SubMenuOption(_ref) {
  var collapse = _ref.collapse,
      exact = _ref.exact,
      icon = _ref.icon,
      name = _ref.name,
      path = _ref.path;
  var classes = useStyles();

  var CollisionLink = _react["default"].forwardRef(function (props, ref) {
    return _react["default"].createElement(_reactRouterDom.NavLink, _extends({
      innerRef: ref,
      to: path
    }, props));
  });

  if (collapse) {
    return _react["default"].createElement(_ListItem["default"], {
      button: true,
      exact: exact,
      activeClassName: classes.chosen,
      component: CollisionLink,
      to: path
    }, _react["default"].createElement(_Tooltip["default"], {
      title: name,
      placement: "right"
    }, _react["default"].createElement(_Icon["default"], {
      className: classes.collapseIcon,
      color: "action"
    }, icon)));
  }

  return _react["default"].createElement(_ListItem["default"], {
    button: true,
    exact: exact,
    activeClassName: classes.chosen,
    component: CollisionLink,
    to: path
  }, _react["default"].createElement(_ListItemIcon["default"], null, _react["default"].createElement(_Icon["default"], null, icon)), _react["default"].createElement(_ListItemText["default"], {
    primary: name
  }));
}

SubMenuOption.defaultProps = {
  collapse: false,
  exact: false
};
SubMenuOption.propTypes = {
  collapse: _propTypes["default"].bool,
  exact: _propTypes["default"].bool,
  icon: _propTypes["default"].node,
  name: _propTypes["default"].string.isRequired,
  path: _propTypes["default"].string.isRequired
};
var _default = SubMenuOption;
exports["default"] = _default;