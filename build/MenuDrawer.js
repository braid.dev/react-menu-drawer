"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Paper = _interopRequireDefault(require("@material-ui/core/Paper"));

var _Divider = _interopRequireDefault(require("@material-ui/core/Divider"));

var _List = _interopRequireDefault(require("@material-ui/core/List"));

var _ListItem = _interopRequireDefault(require("@material-ui/core/ListItem"));

var _Tooltip = _interopRequireDefault(require("@material-ui/core/Tooltip"));

var _styles = require("@material-ui/core/styles");

var _ChevronRight = _interopRequireDefault(require("@material-ui/icons/ChevronRight"));

var _MainMenuOption = _interopRequireDefault(require("./MainMenuOption"));

var _SubMenuOption = _interopRequireDefault(require("./SubMenuOption"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    collapseIconClose: {
      transform: 'rotate(0deg)'
    },
    collapseIconOpen: {
      transform: 'rotate(180deg)',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.leavingScreen
      })
    },
    collapseToggle: {
      justifyContent: 'flex-end'
    },
    divider: {
      marginBottom: theme.spacing(1),
      marginTop: theme.spacing(1)
    },
    mainMenu: {
      backgroundColor: theme.palette.grey[400],
      padding: theme.spacing(1),
      width: 80
    },
    menuDrawer: function menuDrawer(width) {
      return {
        display: 'flex',
        flexDirection: 'row',
        height: '100%',
        minHeight: '100vh',
        position: 'fixed',
        transition: theme.transitions.create('width', {
          duration: theme.transitions.duration.enteringScreen,
          easing: theme.transitions.easing.sharp
        }),
        width: width,
        zIndex: 1099
      };
    },
    subMenu: {
      flex: 1
    }
  };
});

function MenuDrawer(_ref) {
  var collapse = _ref.collapse,
      onCollapse = _ref.onCollapse,
      mainMenuOptions = _ref.mainMenuOptions,
      subMenuOptions = _ref.subMenuOptions,
      width = _ref.width;
  var classes = useStyles(width);

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      mainMenu = _useState2[0],
      setMainMenu = _useState2[1];

  var _useState3 = (0, _react.useState)([]),
      _useState4 = _slicedToArray(_useState3, 2),
      subMenu = _useState4[0],
      setSubMenu = _useState4[1];

  (0, _react.useEffect)(function () {
    var newMainMenu = [];

    for (var i = 0; i < mainMenuOptions.length; i++) {
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = mainMenuOptions[i][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var o = _step.value;
          newMainMenu.push(_react["default"].createElement(_MainMenuOption["default"], {
            key: o.path,
            abbr: o.abbr,
            name: o.name,
            path: o.path,
            icon: o.icon
          }));
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      if (i < mainMenuOptions.length - 1) {
        newMainMenu.push(_react["default"].createElement(_Divider["default"], {
          key: "mainMenu-divider-".concat(i),
          variant: "middle",
          className: classes.divider
        }));
      }

      setMainMenu(newMainMenu);
    }
  }, [mainMenuOptions, classes.divider]);
  (0, _react.useEffect)(function () {
    if (!subMenuOptions) return;
    var newSubMenu = [];

    for (var i = 0; i < subMenuOptions.length; i++) {
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = subMenuOptions[i][Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var o = _step2.value;
          newSubMenu.push(_react["default"].createElement(_SubMenuOption["default"], {
            key: o.path,
            collapse: collapse,
            exact: o.exact,
            name: o.name,
            path: o.path,
            icon: o.icon
          }));
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
            _iterator2["return"]();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      if (i < subMenuOptions.length - 1) {
        newSubMenu.push(_react["default"].createElement(_Divider["default"], {
          key: "subMenu-divider-".concat(i),
          className: classes.divider
        }));
      }
    }

    setSubMenu(newSubMenu);
  }, [collapse, subMenuOptions, classes.divider]);

  function togglecollapse() {
    onCollapse();
  }

  var collapseTip = collapse ? 'Expand Menu' : 'Collapse Menu';
  return _react["default"].createElement(_Paper["default"], {
    className: classes.menuDrawer,
    component: "aside"
  }, _react["default"].createElement("div", {
    className: classes.mainMenu
  }, mainMenu), subMenuOptions && _react["default"].createElement("div", {
    className: classes.subMenu
  }, _react["default"].createElement(_List["default"], {
    component: "nav"
  }, _react["default"].createElement(_ListItem["default"], {
    button: true,
    className: classes.collapseToggle,
    onClick: togglecollapse
  }, _react["default"].createElement(_Tooltip["default"], {
    title: collapseTip,
    placement: "right"
  }, _react["default"].createElement(_ChevronRight["default"], {
    className: collapse ? classes.collapseIconClose : classes.collapseIconOpen,
    color: "action"
  })))), _react["default"].createElement(_Divider["default"], {
    className: classes.divider
  }), subMenu));
}

MenuDrawer.defaultProps = {
  width: 80
};
MenuDrawer.propTypes = {
  collapse: _propTypes["default"].bool,
  mainMenuOptions: _propTypes["default"].array.isRequired,
  onCollapse: _propTypes["default"].func,
  subMenuOptions: _propTypes["default"].array,
  width: _propTypes["default"].number
};
var _default = MenuDrawer;
exports["default"] = _default;