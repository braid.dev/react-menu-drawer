"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactRouterDom = require("react-router-dom");

var _Avatar = _interopRequireDefault(require("@material-ui/core/Avatar"));

var _Icon = _interopRequireDefault(require("@material-ui/core/Icon"));

var _Button = _interopRequireDefault(require("@material-ui/core/Button"));

var _Tooltip = _interopRequireDefault(require("@material-ui/core/Tooltip"));

var _styles = require("@material-ui/core/styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    avatar: {
      backgroundColor: theme.palette.grey[200],
      color: theme.palette.grey[800],
      height: 48,
      width: 48
    },
    chosen: {
      backgroundColor: 'rgba(0,0,0,0.1)'
    }
  };
});

function MainMenuOption(_ref) {
  var abbr = _ref.abbr,
      icon = _ref.icon,
      name = _ref.name,
      path = _ref.path;
  var classes = useStyles();

  var CollisionLink = _react["default"].forwardRef(function (props, ref) {
    return _react["default"].createElement(_reactRouterDom.NavLink, _extends({
      innerRef: ref,
      to: path
    }, props));
  });

  return _react["default"].createElement(_Tooltip["default"], {
    title: name,
    placement: "right"
  }, _react["default"].createElement(_Button["default"], {
    activeClassName: classes.chosen,
    component: CollisionLink,
    className: classes.avatarButton
  }, _react["default"].createElement(_Avatar["default"], {
    className: classes.avatar
  }, icon && _react["default"].createElement(_Icon["default"], null, icon), !icon && !abbr && name.slice(0, 1), !icon && abbr)));
}

MainMenuOption.propTypes = {
  abbr: _propTypes["default"].string,
  icon: _propTypes["default"].node,
  name: _propTypes["default"].string.isRequired,
  path: _propTypes["default"].string.isRequired
};
var _default = MainMenuOption;
exports["default"] = _default;