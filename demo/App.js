import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { matchPath, withRouter } from 'react-router';
import { Switch, Route } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Container from '@material-ui/core/Container';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SpeakerGroupIcon from '@material-ui/icons/SpeakerGroup';
import { makeStyles } from '@material-ui/styles';

import MenuDrawer from '../src/MenuDrawer';

const useStyles = makeStyles(theme => ({
  avatar: {
    backgroundColor: theme.palette.grey[200],
    color: theme.palette.grey[800],
    height: 48,
    width: 48,
  },
  chosen: {
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  grow: {
    flexGrow: 1,
  },
  main: marginLeft => ({
    alignItems: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    height: '100%',
    marginLeft: marginLeft,
    marginTop: theme.spacing(3),
    minHeight: '100vh',
    transition: theme.transitions.create('margin-left', {
      duration: theme.transitions.duration.leavingScreen,
      easing: theme.transitions.easing.sharp,
    }),
  }),
}));

const mainMenu = [
  [
    {
      icon: 'home',
      name: 'Home',
      path: '/home',
    },
  ],
  [
    {
      icon: 'group_work',
      name: 'Team',
      path: '/team',
    },
    {
      name: 'Company',
      path: '/company',
    },
    {
      abbr: 'SD',
      name: 'Software Development',
      path: '/software',
    },
    {
      icon: <SpeakerGroupIcon />,
      name: 'Speaker Group',
      path: '/sg',
    },
  ],
];

function subMenuOptions(path) {
  return [
    [
      {
        exact: true,
        icon: 'home',
        name: 'Home',
        path: `${path}`,
      },
    ],
    [
      {
        icon: 'local_shipping',
        name: 'Orders',
        path: `${path}/orders`,
      },
      {
        icon: 'account_circle',
        name: 'Users',
        path: `${path}/users`,
      },
      {
        icon: 'settings',
        name: 'Settings',
        path: `${path}/settings`,
      },
    ],
  ];
}

function App({location}) {
  const [subMenu, setSubMenu] = useState();
  const [collapse, setCollapse] = useState(false);
  const menuWidth = (subMenuOptions && !collapse) ? 280 : (subMenuOptions && collapse) ? 140 : 80;
  const classes = useStyles(menuWidth);

  useEffect(() => {
    const matchedProgram = matchPath(location.pathname, {path: '/home'});
    if (matchedProgram) {
      setSubMenu(subMenuOptions('/home'));
      return;
    }
    setSubMenu(undefined);
  }, [location]);

  function onCollapse() {
    setCollapse(!collapse);
  }

  return (
    <>
    <MenuDrawer
      collapse={collapse}
      onCollapse={onCollapse}
      width={menuWidth}
      mainMenuOptions={mainMenu}
      subMenuOptions={subMenu}
    />
    <AppBar position="fixed">
      <Toolbar>
        <Typography>
          Brand Logo
        </Typography>
        <div className={classes.grow} />
        <Typography>
          Welcome User
        </Typography>
      </Toolbar>
    </AppBar>
    <main className={classes.main}>
      <Switch>
        <Route path="/home/orders" component={Orders} />
        <Route path="/home/users" component={Users} />
        <Route path="/home/settings" component={Settings} />
        <Route exact path="/home" component={Home} />
      </Switch>
    </main>
    </>
  );
}

App.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default withRouter(App);

function Content({children}) {
  return (
    <Container maxWidth="md">
      <Typography component="h1" variant="h4" gutterBottom>
        {children}
      </Typography>
    </Container>
  );
}

Content.propTypes = {
  children: PropTypes.node.isRequired,
};

const Home = () => <Content>Home</Content>;
const Orders = () => <Content>Orders</Content>;
const Settings = () => <Content>Settings</Content>;
const Users = () => <Content>Users</Content>;
