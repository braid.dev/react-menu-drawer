const path = require('path');

module.exports = {
  devServer: {
    compress: true,
    contentBase: path.join(__dirname, 'demo'),
    historyApiFallback: true,
    port: 9000,
    progress: true,
    watchContentBase: true,
  },
  entry: './demo/index.js',
  mode: 'development',
  module: {
    rules: [
      {
        exclude: /(node_modules|bower_components)/,
        test: /\.(js|mjs|jsx)$/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  output: {
    filename: 'demo.js',
    path: path.join(__dirname, 'demo'),
  },
};
