const path = require('path');

const OFF = 0;
const WARN = 1;
const ERROR = 2;

module.exports = {
  'env': {
    'es6': true,
    'browser': true,
    'node': true,
  },
  'extends': [
    'eslint:recommended',
    // https://github.com/benmosher/eslint-plugin-import
    // used in conjunction with react/forbid-foreign-prop-types
    'plugin:import/errors',
    'plugin:import/warnings',
    // https://github.com/yannickcr/eslint-plugin-react#recommended
    'plugin:react/recommended',
  ],
  'globals': {
    '__ENTRY__': 'readonly',
  },
  'parser': 'babel-eslint',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true,
    }
  },
  'plugins': [
    'react-hooks',
    'react',
  ],
  'rules': {
    // https://github.com/yannickcr/eslint-plugin-react#list-of-supported-rules
    // 'react/forbid-prop-types': WARN,
    'react/forbid-foreign-prop-types': ERROR,
    'react/no-access-state-in-setstate': ERROR,
    'react/no-array-index-key': ERROR,
    'react/no-did-mount-set-state': ERROR,
    'react/jsx-no-bind': ERROR,
    'react/jsx-no-target-blank': ERROR,

    // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/display-name.md
    // This is causing false negatives
    'react/display-name': [OFF],

    'react-hooks/rules-of-hooks': ERROR,
    'react-hooks/exhaustive-deps': WARN,

    'comma-dangle': [ERROR, 'always-multiline'],
    'comma-spacing': ERROR,
    'comma-style': ERROR,
    'curly': [ERROR, 'multi-line'],
    'generator-star-spacing': [ERROR, 'after'],
    'indent': ['error', ERROR],
    'key-spacing': ERROR,
    'keyword-spacing': ERROR,
    'linebreak-style': ERROR,
    'no-array-constructor': ERROR,
    'no-console': OFF, // override eslint:recommended
    'no-multiple-empty-lines': [ERROR, { max: 1 }],
    'no-new-object': ERROR,
    'no-new-wrappers': ERROR,
    'no-tabs': ERROR,
    'no-trailing-spaces': ERROR,
    'no-var': ERROR,
    'no-unused-vars': [ERROR, { args: 'none' }], // override eslint:recommended
    'no-whitespace-before-property': ERROR,
    'prefer-rest-params': ERROR,
    'prefer-spread': ERROR,
    // 'quote-props': [ERROR, 'consistent'],
    'quotes': [ERROR, 'single', { 'allowTemplateLiterals': true }],
    'rest-spread-spacing': ERROR,
    'semi': ERROR,
    'semi-spacing': ERROR,
    'sort-keys': ERROR,
    'space-before-blocks': ERROR,
    'space-before-function-paren': [ERROR, {
      'anonymous': 'never',
      'asyncArrow': 'always',
      'named': 'never',
    }],
    'switch-colon-spacing': ERROR,
    'yield-star-spacing': [ERROR, 'after'],
  },
  'settings': {
    'react': {
      'version': require('react/package.json').version,
    }
  }
}
