import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import MainMenuOption from './MainMenuOption';
import SubMenuOption from './SubMenuOption';

const useStyles = makeStyles(theme => ({
  collapseIconClose: {
    transform: 'rotate(0deg)',
  },
  collapseIconOpen: {
    transform: 'rotate(180deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  collapseToggle: {
    justifyContent: 'flex-end',
  },
  divider: {
    marginBottom: theme.spacing(1),
    marginTop: theme.spacing(1),
  },
  mainMenu: {
    backgroundColor: theme.palette.grey[400],
    padding: theme.spacing(1),
    width: 80,
  },
  menuDrawer: width => ({
    display: 'flex',
    flexDirection: 'row',
    height: '100%',
    minHeight: '100vh',
    position: 'fixed',
    transition: theme.transitions.create('width', {
      duration: theme.transitions.duration.enteringScreen,
      easing: theme.transitions.easing.sharp,
    }),
    width: width,
    zIndex: 1099,
  }),
  subMenu: {
    flex: 1,
  },
}));

function MenuDrawer({collapse, onCollapse, mainMenuOptions, subMenuOptions, width}) {
  const classes = useStyles(width);
  const [mainMenu, setMainMenu] = useState([]);
  const [subMenu, setSubMenu] = useState([]);

  useEffect(() => {
    const newMainMenu = [];
    for (let i = 0; i < mainMenuOptions.length; i++) {
      for (const o of mainMenuOptions[i]) {
        newMainMenu.push(
          <MainMenuOption key={o.path} abbr={o.abbr} name={o.name} path={o.path} icon={o.icon}/>
        );
      }
      if (i < mainMenuOptions.length - 1) {
        newMainMenu.push(
          <Divider key={`mainMenu-divider-${i}`} variant="middle" className={classes.divider} />
        );
      }
      setMainMenu(newMainMenu);
    }
  }, [mainMenuOptions, classes.divider]);

  useEffect(() => {
    if (!subMenuOptions) return;
    const newSubMenu = [];
    for (let i = 0; i < subMenuOptions.length; i++) {
      for (const o of subMenuOptions[i]) {
        newSubMenu.push(
          <SubMenuOption key={o.path} collapse={collapse} exact={o.exact} name={o.name} path={o.path} icon={o.icon}/>
        );
      }
      if (i < subMenuOptions.length - 1) {
        newSubMenu.push(
          <Divider key={`subMenu-divider-${i}`} className={classes.divider} />
        );
      }
    }
    setSubMenu(newSubMenu);
  }, [collapse, subMenuOptions, classes.divider]);

  function togglecollapse() {
    onCollapse();
  }

  const collapseTip = collapse ? 'Expand Menu' : 'Collapse Menu';

  return (
    <Paper className={classes.menuDrawer} component="aside">
      <div className={classes.mainMenu}>
        {mainMenu}
      </div>
      {subMenuOptions &&
      <div className={classes.subMenu}>
        <List component="nav">
          <ListItem
            button
            className={classes.collapseToggle}
            onClick={togglecollapse}>
            <Tooltip title={collapseTip} placement="right">
              <ChevronRightIcon className={collapse ? classes.collapseIconClose : classes.collapseIconOpen} color="action" />
            </Tooltip>
          </ListItem>
        </List>
        <Divider className={classes.divider} />
        {subMenu}
      </div>
      }
    </Paper>
  );
}

MenuDrawer.defaultProps = {
  width: 80,
};

MenuDrawer.propTypes = {
  collapse: PropTypes.bool,
  mainMenuOptions: PropTypes.array.isRequired,
  onCollapse: PropTypes.func,
  subMenuOptions: PropTypes.array,
  width: PropTypes.number,
};

export default MenuDrawer;
