import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider, makeStyles } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

import App from './App';

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    error: {
      main: red.A400,
    },
    primary: {
      main: '#1976d2',
    },
    secondary: {
      main: '#388e3c',
    },
  },
});

const htmlBody = {
  border: 0,
  height: '100%',
  margin: 0,
  minHeight: '100%',
  outline: 0,
  padding: 0,
};

const useStyles = makeStyles(theme => ({
  '@global': {
    '#app': {
      display: 'flex',
      minHeight: '100%',
      paddingTop: 64,
      position: 'relative',
    },
    body: {
      ...htmlBody,
    },
    html: {
      ...htmlBody,
    },
  },
}));

function Root() {
  useStyles();
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </BrowserRouter>
  );
}

ReactDOM.render(
  <Root/>,
  document.querySelector('#app'),
);
