import React from 'react';
import PropTypes from 'prop-types';
import { NavLink as RouterLink } from 'react-router-dom';

import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  avatar: {
    backgroundColor: theme.palette.grey[200],
    color: theme.palette.grey[800],
    height: 48,
    width: 48,
  },
  chosen: {
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
}));

function MainMenuOption({abbr, icon, name, path}) {
  const classes = useStyles();
  const CollisionLink = React.forwardRef((props, ref) => (
    <RouterLink innerRef={ref} to={path} {...props} />
  ));
  return (
    <Tooltip title={name} placement="right">
      <Button
        activeClassName={classes.chosen}
        component={CollisionLink}
        className={classes.avatarButton}>
        <Avatar className={classes.avatar}>
          {icon &&
            <Icon>{icon}</Icon>
          }
          {!icon && !abbr && name.slice(0, 1)}
          {!icon && abbr}
        </Avatar>
      </Button>
    </Tooltip>
  );
}

MainMenuOption.propTypes = {
  abbr: PropTypes.string,
  icon: PropTypes.node,
  name: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

export default MainMenuOption;
